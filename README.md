# BHACK 2020 : Blind SQL Injections
__Presenter :__ Robert H. Osborne<br>
__Alias :__ tobor<br>
__Contact :__ rosborne@osbornepro.com<br>

### Blind SQL Injection References
- [PortSwigger](https://portswigger.net/web-security/sql-injection/blind)
- [OWASP](https://owasp.org/www-community/attacks/Blind_SQL_Injection)

### SUMMARY
This docker image was created for my presentation at BHack 2020. Check my GitHub or GitLab if you wish to obtain the project from there.
In the presentation I am going to cover Blind SQL Injections. To do this I have built a vulnerable web application using PHP, MySQL, and Apache on an Alpine Linux host.<br>
> A Blind SQL Injection is a SQL Injection that returns a True or False value to your SQL query.
There will NOT be any output returned in the HTML response containing information from the SQL database.
The value of True or False can be determined by an error message that is returned from the application. 
The error message could be some type of HTML error or as in the case I will demonstrate; a string of text verifying a successful or failed query.
To perform a Blind SQL Injection attack our queries will need to be true or false questions.

---
# Getting Started

### ENABLE FIREWALL (_OPTIONAL_)
If you prefer to access the application from a separate attack machine, the following commands will open port 80.
##### Firewalld
```bash
sudo firewall-cmd --permanent --zone=public --add-port=80/tcp
sudo firewall-cmd --reload
```

##### IPTables
```bash
sudo iptables -I INPUT 5 -i eth0 -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo service iptables save
```
##### UFW
```bash
sudo ufw allow http
sudo ufw reload
```
---

### INSTALL VULNERABLE APPLICATION
Too install the vulnerable web application perform the following actions:

__Install Docker__
```bash
sudo apt update
sudo apt install docker.io
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

__Start docker service and enable it to start automatically__
```bash
sudo systemctl start docker
sudo systemctl enable docker
```

__Download the docker image__
```bash
# sudo docker pull toborobot/bhack2020_blindsqli:latest
git clone https://github.com/tobor88/BHack2020-BlindSQLi
```

Run the image. DO NOT change the port as it is configured to run on port 80
```bash
# From the root of this project run the below command
cd BHack2020-BlindSQLi
sudo docker-compose start
sudo docker-compose up
```

With the above tree structure run and _docker.io_ and _docker-compose_ installed locally run ```docker-compose up``` from the root of this project. The site will then be accessible on your local machine from [http://localhost:80/](http://localhost:80)

 __My Links :__<br>
- [Robert H. Osborne Site](https://roberthosborne.com)
- [OsbornePro](https://osbornepro.com)
- [GitHub](https://github.com/tobor88)
- [GitLab](https://gitlab.com/tobor88)
- [PS Gallery](https://www.powershellgallery.com/profiles/tobor)
- [HackTheBox](https://www.hackthebox.eu/profile/52286)
- [LinkedIn](https://www.linkedin.com/in/roberthosborne/)
- [Acclaim Badges](https://www.youracclaim.com/users/roberthosborne/badges)
