CREATE DATABASE IF NOT EXISTS ceh;
USE ceh;
CREATE TABLE IF NOT EXISTS tvshow (
	id int auto_increment,
	name varchar(50),
	superpower varchar(50),
	class int,
	primary key (id)
);
INSERT INTO tvshow (name, superpower, class) VALUES ('David Haller', 'Godlike', 5);
INSERT INTO tvshow (name, superpower, class) VALUES ('Syd Barrett', 'Soul Trader', 3);
INSERT INTO tvshow (name, superpower, class) VALUES ('Lenny Busker', 'Bi-polar', 1);
INSERT INTO tvshow (name, superpower, class) VALUES ('Amahal Farouk', 'Godlike', 5);
INSERT INTO tvshow (name, superpower, class) VALUES ('Ptonomy Wallace', 'Dream Eater', 3);
INSERT INTO tvshow (name, superpower, class) VALUES ('Kerry Loudermilk', 'Intelligent', 1);
INSERT INTO tvshow (name, superpower, class) VALUES ('Melanie Bird', 'Telepath', 3);
INSERT INTO tvshow (name, superpower, class) VALUES ('Amy Haller', 'Normal', 1);
INSERT INTO tvshow (name, superpower, class) VALUES ('Cary Loudermilk', 'Ju Jitsu', 1);
INSERT INTO tvshow (name, superpower, class) VALUES ('Jia Yi', 'Fourth Dimensional Being', 6);
