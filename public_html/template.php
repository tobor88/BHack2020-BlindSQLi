<?php
session_start();
$_SESSION['page'] = "template.php";
?>
<html>
<head>
<title>The Blind SQLi Web App</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>
<header>
<h1>BHack 2020 Blind SQL Injection</h1>
<nav><ul>
<li><a href="template.php">Home</a></li>
<li><a href="database.php">Database</a></li>
</ul></nav>
</header>
<center><img src="images/legion.png" alt="I am Legion" /></center>
<footer>
The Blind SQLi Web App &copy; Robert H. Osborne : BHack 2020
</footer>
</body>
</html>
